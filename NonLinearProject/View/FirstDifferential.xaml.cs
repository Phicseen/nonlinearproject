﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NonLinearProject.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FirstDifferential : ContentPage
    {
        public FirstDifferential(string originalEquation)
        {
            InitializeComponent();
            BindingContext = new ViewModel.FirstDifferentialVm(originalEquation);
        }
    }
}