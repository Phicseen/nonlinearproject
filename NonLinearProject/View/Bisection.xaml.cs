﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NonLinearProject.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Bisection : ContentPage
    {
        public Bisection()
        {
            InitializeComponent();
            BindingContext = new ViewModel.BisectionVn();
        }
    }
}