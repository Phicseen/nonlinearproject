﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace NonLinearProject
{
    public static class BisectionSolver
    {
        public delegate int Print(int x);

        public static double solve(double a, double b,double E, string Equation)
        {
            double m = 0;
            double answer = 0;
            var fb = Evaluate(Equation, b);
            var fa = Evaluate(Equation, a);

            var check = fa * fb;
            if (check > 0)
            {
                Application.Current.MainPage.DisplayAlert("Error", "Function has same signs at ends of interval", "ok");
                return 0;
            }
            else
            {
                while (Math.Abs(b - a) > E)
                {

                    m = (a + b) / 2;
                    if (Math.Abs(b - a) < E)
                    {
                        answer = m;
                        break;
                    }
                        if (Evaluate(Equation, a) * Evaluate(Equation, m) < 0.0D)
                    {
                        b = m;
                    }
                    else
                    {
                        a = m;
                    }


                }
                return m;
               
            }

           

        }



        public static double Evaluate(string expression, double x)
        {
            char[] tokens = expression.ToCharArray();
            // operend stack
            Stack<double> values = new Stack<double>();

            // operator stack
            Stack<char> operations = new Stack<char>();

            for (int i = 0; i < tokens.Length; i++)
            {
                var tok = tokens[i];
                //  skip if current token is a white space
                if (tokens[i] == ' ')
                {
                    continue;
                }

                // if current token is a number push it to values
                if (tokens[i] >= '0' && tokens[i] <= '9')
                {

                    StringBuilder builder = new StringBuilder();
                    while (i < tokens.Length && tokens[i] >= '0' && tokens[i] <= '9')
                    {
                        builder.Append(tokens[i++]);
                    }
                    values.Push(double.Parse(builder.ToString()));
                    i--;


                    if (operations.Count > 0)
                    {
                        if (operations.Peek() == '-')
                        {
                            if (values.Count == 1)
                            {
                                values.Pop();
                                operations.Pop();
                                values.Push(-1 * double.Parse(builder.ToString()));

                            }
                        }
                    }
                    var var = values.Peek();
                }
                else if (tokens[i] == 'x')
                {
                    values.Push(x);
                }
                else if (tokens[i] == 'e')
                {
                    values.Push(2.718);
                }

                // if current opening is an opening braces push it to operations
                else if (tokens[i] == '(')
                {
                    operations.Push(tokens[i]);
                }

                // if current opening is a closing braces solve the entire brace
                else if (tokens[i] == ')')
                {

                    while (operations.Peek() != '(')
                    {
                        if (operations.Peek() == 's' || operations.Peek() == 't' || operations.Peek() == 'c')
                        {
                            values.Push(appllytangent(operations.Pop(), values.Pop()));
                        }
                        else
                        {
                            values.Push(applyop(operations.Pop(), values.Pop(), values.Pop()));

                        }
                    }
                    operations.Pop();

                }
                else if (tokens[i] == 's' || tokens[i] == 't' || tokens[i] == 'c')
                {
                    operations.Push(tokens[i]);
                }
                // if current toking is an operator
                else if (tokens[i] == '+' || tokens[i] == '-' || tokens[i] == '*' || tokens[i] == '/' || tokens[i] == '^')
                {
                    var ee = tokens[i];

                    while (operations.Count > 0 && HasPrecedence(tokens[i], operations.Peek()))
                    {
                        // var sq = operations.Peek();
                        if (operations.Peek() == 's' || operations.Peek() == 't' || operations.Peek() == 'c')
                        {
                            values.Push(appllytangent(operations.Pop(), values.Pop()));
                            var aa = values.Peek();
                            if (operations.Count > 0)
                            {
                                if (operations.Peek() == '-')
                                {
                                    if (values.Count == 1)
                                    {
                                        //  values.Pop();
                                        operations.Pop();
                                        values.Pop();
                                        values.Push(-1 * aa);
                                    }
                                }
                            }
                        }
                        else
                        {
                            values.Push(applyop(operations.Pop(), values.Pop(), values.Pop()));

                        }

                    }

                    operations.Push(tokens[i]);
                }

            }



            while (operations.Count > 0)
            {
                if (operations.Peek() == 's' || operations.Peek() == 't' || operations.Peek() == 'c')
                {

                    values.Push(appllytangent(operations.Pop(), values.Pop()));
                    var aa = values.Peek();
                    if (operations.Count > 0)
                    {
                        if (operations.Peek() == '-')
                        {
                            if (values.Count == 1)
                            {
                                //  values.Pop();
                                operations.Pop();
                                values.Pop();
                                values.Push(-1 * aa);
                            }
                        }
                    }
                }
                else
                {
                    var si = operations.Peek();

                    values.Push(applyop(operations.Pop(), values.Pop(), values.Pop()));
                    var ss = values.Peek();

                }
            }
            //var ee = values.Pop();
            return values.Pop();

        }

        // end of evaluate function




        public static bool HasPrecedence(char operatorone, char operatortwo)
        {
            if (operatorone == '(' || operatortwo == ')')
            {
                return false;
            }

            else if ((operatorone == '/' || operatorone == '*' || operatorone == '+' || operatorone == '-') && (operatortwo == '^'))
            {
                return true;
            }
            else if ((operatorone == '/' || operatorone == '*' || operatorone == '+' || operatorone == '-' || operatorone == '^') && (operatortwo == 's') || operatortwo == 't' || operatortwo == 'c')
            {
                return true;
            }
            else if
             ((operatortwo == '*' || operatortwo == '/') && (operatorone == '+' || operatorone == '-'))
            {
                return true;
            }
            else if (operatorone == '+' && operatortwo == '-')
            {
                return true;
            }
            else if (operatorone == '-' && operatortwo == '+')
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public static double appllytangent(char op, double a)
        {
            switch (op)
            {

                case 's':
                    return Math.Sin(a);
                case 't':
                    return Math.Tan(a);
                case 'c':
                    return Math.Cos(a);
                case 'e':
                    return Math.Exp(a);

            }
            return 0;
        }





        public static double applyop(char op, double b, double a)
        {
            switch (op)
            {
                case '+':
                    return a + b;
                case '-':
                    return a - b;
                case '*':
                    return a * b;
                case '/':
                    if (b == 0)
                    {
                        throw new System.NotSupportedException("Cannot divide by zero");
                    }
                    return a / b;
                case '^':
                    return Math.Pow(a, b);

            }
            return 0;
        }
    }
}
