﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NonLinearProject
{
    public interface Imessage
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
