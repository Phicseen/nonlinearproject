﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace NonLinearProject.ViewModel
{
    public class FirstDifferentialVm : BaseViewModel
    {
        string _ValueA;
        public string ValueA {
            get
            {
                return _ValueA;
            }
            set
            {
                SetProperty(ref _ValueA, value);
            }
        
        }

        string _ValueB;
        public string ValueB
        {
            get
            {
                return _ValueB;
            }
            set
            {
                SetProperty(ref _ValueB, value);
            }

        }
        Imessage Toast => DependencyService.Get<Imessage>();
        string equation;
        public string Equation
        {
            get
            {
                return equation;
            }
            set
            {
                SetProperty(ref equation, value);
            }
        }

        string original;

        public FirstDifferentialVm(string originalEquation)
        {
            original = originalEquation;
        }
        string originalequation;
        public string OriginalEquation
        {
            get
            {
                return originalequation;
            }
            set
            {
                SetProperty(ref originalequation, value);
            }
        }

        public Command<string> OperandCommand
        {
            get
            {
                return new Command<string>((parameter) =>
                {
                    if (parameter == "Sin")
                    {
                        Equation += parameter;
                        OriginalEquation += "s";
                    }
                    else if (parameter == "Cos")
                    {
                        Equation += parameter;
                        OriginalEquation += "c";
                    }
                    else if (parameter == "Tan")
                    {
                        Equation += parameter;
                        OriginalEquation += "t";
                    }
                    else if (parameter == "Log")
                    {
                        Equation += parameter;
                        OriginalEquation += "l";
                    }
                    else if (parameter == "E")
                    {
                        Equation += parameter;
                        originalequation += "e";
                    }
                    else
                    {
                        Equation += parameter;
                        OriginalEquation += parameter;
                    }

                });
            }
        }

        public Command DeleteCommand
        {
            get
            {
                return new Command(() =>
                {
                    if (Equation.Length > 0)
                    {
                        var ff = Equation.Substring(0, Equation.Length - 1);
                        Equation = ff;
                        var fs = Equation.Substring(0, OriginalEquation.Length - 1);
                        OriginalEquation = fs;
                    }
                    else
                    {

                    }
                });
            }
        }

        public Command ClearCommand
        {
            get
            {
                return new Command(() =>
                {
                    if (Equation.Length > 0)
                    {
                        var ff = "";
                        Equation = ff;
                        OriginalEquation = ff;
                    }
                    else
                    {

                    }
                });
            }
        }
        public Command NextCommand
        {
            get
            {
                return new Command(() =>
                {

                    if (string.IsNullOrEmpty(OriginalEquation))
                    {
                        Toast.LongAlert("Please enter a valid Equation");

                    }
                    else if (string.IsNullOrEmpty(ValueA)||string.IsNullOrEmpty(ValueB))
                    {
                        Toast.LongAlert("Please enter value A and B");
                    }
                    else
                    {
                        if (!OriginalEquation.Contains("x"))
                        {
                            Toast.LongAlert("Equation must contain x");
                            return;
                        }
                        else
                        {
                            var answer = NewtonRaphsonSolver.solvenewtonraphson(double.Parse(ValueA),double.Parse(ValueB), original,OriginalEquation);
                            Application.Current.MainPage.DisplayAlert("Answer", $"The root of the equation is {answer}", "ok");
                        }
                    }

                });
            }
        }
    }
}

